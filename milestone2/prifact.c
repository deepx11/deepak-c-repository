#include<stdio.h>
#include<math.h>
#include<stdlib.h>
int prifact(int n)
{
	while (n % 2 == 0)
	{ 
        printf("%d ", 2); 
        n = n / 2; 
    } 
    for (int i = 3; i <= sqrt(n); i = i + 2) 
    { 
        while (n % i == 0) 
        { 
            printf("%d ", i); 
            n = n / i; 
        } 
    } 
	if (n > 2) 
        return n;
}
int main()
{
	int num;
	printf("Enter a number:");
	scanf("%d",&num);
	printf("%d\n",prifact(num) );
	return 0;
}
