#include<stdio.h>
#include<math.h>
#include<stdlib.h>
int doubleFV(double rate, unsigned int nperiods, double PV){
    double FV;
    FV= PV*(pow((1+rate),nperiods));
    return FV;
}
int main(){
    double rate,pv;
    unsigned int nperiods;
    printf("enter the rate\n");
    scanf("%lf",&rate);
    printf("enter the periods\n");
    scanf("%u",&nperiods);
    printf("enter the present value");
    scanf("%lf",&pv);
    printf("the future value of the investment is %lf ",doubleFV(rate,nperiods,pv));
    return 0;
}

/*double FV(double rate, unsigned int nperiods, double PV) - 
Calculates and returns the Future Value of an investment 
based on the 
compound interest formula FV = PV * (1+rate)nperiods*/