#include <stdio.h>
 
int factorial(unsigned int n){
    int fact=1,i;
    
    for(i=n;i>=1;i--){
        fact=fact*i;
    } 
    return fact;
}

 
void main()
{
    int n, r, nCr;
 
    printf("Enter the value of n:\n");
    scanf("%d", &n);
    printf("Enter the value of r:\n");
    scanf("%d", &r);
    nCr = factorial(n) / (factorial(r) * factorial(n - r));
    printf(" C(%d,%d)  %d",n,r,nCr);
}

