#include <stdio.h>
#include <stdlib.h>
int main()
{
    int i,j,n,a,array[100],find,choice,new,sortchoice,removed;
    printf("How many elements do you want in the array?\n");
    scanf("%d",&n);
    printf("Enter the elements in the array\n");
    for(i=0;i<n;i++){
        scanf("%d",&array[i]);
    }
    printf("Elements successfully inserted into array \n");
    printf("****************************************\n");
    printf("MENU\n");
    printf("1. ADD element\n");
    printf("2. SEARCH element\n");
    printf("3. REMOVE element\n");
    printf("4. SORT the array\n");
    printf("5. PRINT the array\n");
    printf("6. EXIT\n");
    printf("****************************************\n");
    printf("Choose from 1/2/3/4/5/6\n");
    scanf("%d",&choice);
    switch(choice){
        case 1: /*Adding element*/
                printf("Enter the element you want to add\n");
                scanf("%d",&new);
                for(i=0;i<=n;i++){
                    if(array[i]==new){
                        printf("element already exists");
                        break;
                    }else{
                        n++;
                        array[n-1]=new;
                        printf("Element successfully added\n");
                        for(i=0;i<n;i++){
                            printf("%d\n",array[i]);
                        }
                    }
                }
                break;
        case 2: /*Searching an element*/
                printf("Enter the element you want to find");
                scanf("%d",&find);
                for(i=0;i<=n;i++){
                    if(array[i]==find){
                        printf("element %d found at index %d",find,i);
                        break;
                    }
                }
        case 3: /*Removing an element*/
                printf("enter the element you want to remove");
                scanf("%d",&removed);
                for(i=0;i<n;i++){
                    if(array[i]==removed){
                        printf("Element found and hence removed");
                        break;
                    }else{
                        printf("Element not removed");
                        break;
                    }
                }
                break;
        case 4: /*Sorting the array*/
                printf("how do you want to sort the array\n 1. Ascending\n2. Descending \n");
                scanf("%d",&sortchoice);
                if(sortchoice==1){
                    //Ascending order sort
                    for(i=0;i<n;i++){
                        for(j=i+1;j<n;j++){
                            if(array[i]>array[j]){
                                a=array[i];
                                array[i]=array[j];
                                array[j]=a;
                            }
                        }
                    }printf("The sorted array in Ascending order is\n");
                    for(i=0;i<n;i++){
                        printf("%d\n",array[i]);
                    }
                }else{
                    //for Descending order
                    for(i=0;i<n;i++){
                        for(j=i+1;j<n;j++){
                            if(array[i]<array[j]){
                                a=array[i];
                                array[i]=array[j];
                                array[j]=a;
                            }
                        }
                    }
                    printf("The sorted array in Dscending order is\n");
                    for(i=0;i<n;i++){
                        printf("%d\n",array[i]);
                    }
                }break;
        case 5: printf("The array is\n");
                for(i=0;i<n;i++){
                        printf("%d\n",array[i]);
                    }
                break;
        case 6: printf("Exiting now \n");
                exit(0);
                break;
                
        default : printf("wrong choice");
                    break;
        
    }

    return 0;
}
