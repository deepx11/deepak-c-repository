#include <stdio.h>
int str_copy(char *str1, char *str2);
int main()
{
    int result;
    char str1[20],str2[20];
    printf("Enter a string:");
    scanf("%s",&str1);
    result=str_copy(str1,str2);
    printf("String1 is = %s and String2 is = %s and the result is %d",str1,str2,result);
}
int str_copy(char *str1, char *str2)
{
    char *string1, *string2;
    string1 = str1;
    string2 = str2;
    int flag=0;
    while(*str1 != '\0')
    {
        *str2=*str1;
        str1++;
        str2++;
    }
    *str2 = '\0';
    while(*string1 !='\0' && *string2 !='\0')
    {
       if(*string1 != *string2)
       {
           flag=-1;
           break;
       }
       string1++;
       string2++;
    }
    return flag;
}