int str_find_substring(char *str1, char *str2)
{
    int i=0,j=0,str2length=0,start=0,count=0;
    while(str2[j] != '\0')
    {
        str2length++;
        j++;
    }
    j=0;
    while(str1[i] != '\0' || str2[j] != '\0')
    {
        if(str1[i]==str2[j])
        {
            if(start==0)
            {
                start=i;
            }
            count++;
            i++;
            j++;
        }
        else
        {
            i++;
            if(str2[j] != '\0')
            {
                j=0;
                start=0;
                count=0;
            }
        }
    
    }
    if(count==str2length)
    {
        return start;
    }
    else
    {
        return -1;
    }

}